package com.example.homework1_fragmentsedilberto.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.homework1_fragmentsedilberto.Adapter.ProductoaqpAdapter
import com.example.homework1_fragmentsedilberto.Productoaqp
import com.example.homework1_fragmentsedilberto.R
import kotlinx.android.synthetic.main.fragment_fragmento1.*


class fragmento1 : Fragment() {
    val productos = mutableListOf<Productoaqp>()
    val adaptador by lazy {
        ProductoaqpAdapter(productos)
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragmento1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cargarinformacion()
        configuraradaptador()
    }

    private fun configuraradaptador() {
        recyclerproductos.adapter = adaptador
        recyclerproductos.layoutManager = LinearLayoutManager(requireContext())

    }

    private fun cargarinformacion() {
        productos?.add(Productoaqp("Abarrotes","S/.10","","Los Abarrotes a buen precio "))
        productos?.add(Productoaqp("Verduras","S/.5","","las verduras verdesitas de todo"))
        productos?.add(Productoaqp("Bebidas","S/.3","","las bebidas calientes y frias"))
        productos?.add(Productoaqp("Panaderia","S/.1","","tortas y variedades de panes"))
    }


}