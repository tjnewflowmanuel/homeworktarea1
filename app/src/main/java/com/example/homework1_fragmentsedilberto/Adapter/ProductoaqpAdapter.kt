package com.example.homework1_fragmentsedilberto.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.homework1_fragmentsedilberto.Productoaqp
import com.example.homework1_fragmentsedilberto.R


class ProductoaqpAdapter(var productoaqps: MutableList<Productoaqp>):RecyclerView.Adapter<ProductoaqpAdapter.ProductoAdapterViewHolder>() {

    class ProductoAdapterViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){

        fun bind(productoaqp:Productoaqp){

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductoAdapterViewHolder {
        val view:View = LayoutInflater.from(parent.context).inflate(R.layout.item_productos,parent,false)
        return ProductoAdapterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return productoaqps.size
    }

    override fun onBindViewHolder(holder: ProductoAdapterViewHolder, position: Int) {
        val producto = productoaqps[position]
        holder.bind(producto)
    }


}