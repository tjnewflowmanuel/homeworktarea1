package com.example.homework1_fragmentsedilberto

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    var productoaqps:ArrayList<Productoaqp>?= null
    var nombreProductos:ArrayList<String>?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_fragmento)

        productoaqps = ArrayList()
        productoaqps?.add(Productoaqp("Abarrotes","S/.10","","Los Abarrotes a buen precio "))
        productoaqps?.add(Productoaqp("Verduras","S/.5","","las verduras verdesitas de todo"))
        productoaqps?.add(Productoaqp("Bebidas","S/.3","","las bebidas calientes y frias"))
        productoaqps?.add(Productoaqp("Panaderia","S/.1","","tortas y variedades de panes"))
        nombreProductos = obtenerNombreProductos(productoaqps!!)

    }

    fun obtenerNombreProductos(productoaqps: ArrayList<Productoaqp>): ArrayList<String>? {
val nombres = ArrayList<String>()
        for (producto in productoaqps){
            nombres.add(producto.nombre)
        }
        return nombres
    }
}